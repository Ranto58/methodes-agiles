-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 01 oct. 2019 à 10:21
-- Version du serveur :  5.7.23
-- Version de PHP :  7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `menuiserie`
--

-- --------------------------------------------------------

--
-- Structure de la table `experiences`
--

DROP TABLE IF EXISTS `experiences`;
CREATE TABLE IF NOT EXISTS `experiences` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Titre` varchar(20) NOT NULL,
  `Date` date NOT NULL,
  `Lieu` varchar(15) NOT NULL,
  `Description` text NOT NULL,
  `Employeur` varchar(15) NOT NULL,
  `Image` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `experiences`
--

INSERT INTO `experiences` (`ID`, `Titre`, `Date`, `Lieu`, `Description`, `Employeur`, `Image`) VALUES
(1, 'Menuisier', '2017-10-04', 'Toulouse', 'Travail sur des meubles en bois de chêne sur mesure', 'Mobalpa', 'https://www.mobalpa.fr/sites/mobalpa/files/redactionnel/focus/2018/06/mobalpa-cuisine-equipee-bois-clair-modele-sur-mesure-moderne-brut_0004_mob-cui-pavola-vp.jpg'),
(2, 'Menuisier', '2018-08-08', 'Toulouse', 'Travail sur meuble avec encastrement chez particulier.', 'Centrakor', 'https://moya-kuhnya.com/wp-content/uploads/2018/08/astuce-rangement-placard-cuisine-elegant-astuce-rangement-placard-cuisine-inspirant-amenagement-interieur-of-astuce-rangement-placard-cuisine.jpg'),
(3, 'Menuisier', '2019-08-07', 'Montauban', 'Travail en auto entrepreneur. Création de mobilier sur mesure avec possible encastration.', 'Emeric Lebon', 'https://www.paris-prix.com/90625-large_paris/meuble-tv-design-silk-240cm-gris-naturel.jpg');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
