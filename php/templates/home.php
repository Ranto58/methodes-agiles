<section id="images">
</section>
<section id="content">
  <section class="profile">
    <div class="flex">
      <img src="assets/img/profile.png" alt="Profile" />
      <div class="infos">
        <h2>M. Lebon</h2>
        <span>TOULOUSE Menuiserie</span>
        <span class="muted">Meubles encastrés, sur mesure</span>
      </div>
    </div>
    <div class="infos">
      <h2 class="contacts">Contacts</h2>
      <span>Tel : 06 75 80 85 90</span>
      <span>20 Impasse Charles Fournier</span>
    </div>
  </section>
  <section id="experiences">
  <?php foreach(getExperiences() as $exp){ ?>
    <article>
      <h3><?= $exp["Titre"]; ?></h3>
      <div> 
        <span><?= $exp["Employeur"]; ?></span>
      </div>
      <div>
        <span><?= $exp["Description"]; ?></span>
        <img src="<?= $exp['Image']; ?>" />
      </div>
    </article>
  <?php
  }; ?>
</section>
</section>
