<?php 
function getExperiences(){
  global $db;
  $experiences = [];
  $req = $db->prepare("
  SELECT *
  FROM experiences");
  $req->execute();
  while($res = $req->fetch()){
      array_push($experiences, $res);
  }
  return $experiences;
}