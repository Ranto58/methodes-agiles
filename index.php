
<?php include("php/models/db.php"); ?>
<?php include("php/models/models.php"); ?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="description" value="Menuisier sur Toulouse pour vos meubles sur mesures et encastrés">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>M. Lebon - Menuiserie Toulouse</title>
  <meta property="og.title" content="M. Lebon - Menuiserie Toulouse"/>
  <meta property="og:type" content="website"/>
  <meta property="pg:description" content="Menuiserie M. Lebon situé à Toulouse
  vous propose ses services de menuiserie, nous sommes experts dans l'art des
  meubles, du sur-mesures et de l'encastration."/>
  <meta property="og:url" content="http://www.lebon.fr" />
  <link rel="stylesheet" href="assets/css/master.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
</head>
<body>
  <?php include("php/templates/layout/header.php"); ?>
  <?php include("php/templates/home.php"); ?>
</body>
</html>
